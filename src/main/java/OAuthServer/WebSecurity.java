package OAuthServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by rahul on 2/5/16.
 */
@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication()
                .withUser("rahul").password("password").roles("ADMIN")
                .and()
                .withUser("sriram").password("password").roles("USER");
    }


    @Override
    public void configure(HttpSecurity http) throws Exception {

        //not working ???
        http
                .requestMatchers().antMatchers("/user").and()
                .authorizeRequests()
                .anyRequest().permitAll(); //[4]
    }

}